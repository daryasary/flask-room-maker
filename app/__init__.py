import logging.config
from os import environ

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from .config import config as app_config

db = SQLAlchemy()


def create_app():
    __APPLICATION_ENV = get_environment()
    logging.config.dictConfig(app_config[__APPLICATION_ENV].LOGGING)
    app = Flask(app_config[__APPLICATION_ENV].APP_NAME)
    app.config.from_object(app_config[__APPLICATION_ENV])

    db.init_app(app)

    from .core.models import User, Organization, MeetingRoom
    with app.app_context():
        db.create_all()

    CORS(app, resources={r'/api/*': {'origins': '*'}})

    from .core.views import bp as core_blueprint
    app.register_blueprint(
        core_blueprint,
        url_prefix='/api/v1/rooms'
    )

    return app


def get_environment():
    return environ.get('__APPLICATION_ENV') or 'development'

import random
import string

from sqlalchemy import func

from app import db


class BaseModelMixin:
    id = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.id}"

    @staticmethod
    def store(instance):
        db.session.add(instance)
        db.session.commit()

    @staticmethod
    def delete(instance):
        db.session.delete(instance)
        db.session.commit()


class Organization(db.Model, BaseModelMixin):
    name = db.Column(db.String(32), unique=True, nullable=False)

    @classmethod
    def get_or_create_sample(cls):
        organization = cls.query.first()
        if organization is None:
            organization = cls(name='Test')
            cls.store(organization)
        return organization


class User(db.Model, BaseModelMixin):
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    organization_id = db.Column(
        db.Integer, db.ForeignKey('organization.id'), nullable=False
    )
    organization = db.relationship(
        'Organization', backref=db.backref('users', lazy=True)
    )

    @classmethod
    def get_or_create_sample(cls):
        user = cls.query.first()
        if user is None:
            organization = Organization.get_or_create_sample()
            user = cls(
                username="admin", email='amin@mail.com',
                organization=organization
            )
            cls.store(user)
        return user


class MeetingRoom(db.Model, BaseModelMixin):
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False
    )
    user = db.relationship(
        'User', backref=db.backref('meetings', lazy=True)
    )
    name = db.Column(db.String(32), unique=True, nullable=False)
    organization_id = db.Column(
        db.Integer, db.ForeignKey('organization.id'), nullable=False
    )
    organization = db.relationship(
        'Organization', backref=db.backref('meetings', lazy=True)
    )

    created_at = db.Column(
        db.DateTime(timezone=True), server_default=func.now()
    )

    __json_fields = ['id', 'user_id', 'name', 'organization_id', 'created_at']

    @classmethod
    def load_rooms(cls, organization_id=None):
        if organization_id:
            organization = Organization.query.filter(id=organization_id)
            if organization is not None:
                return organization.meetings.all()
            return []
        return cls.query.all()

    @classmethod
    def create(cls, data):
        # Since authentication method is not implemented yet using
        # sample data here and save rooms for User(1)
        user = User.get_or_create_sample()
        name = data.get(
            'name', ''.join(random.sample(string.ascii_letters, 6))
        )
        while cls.query.filter_by(name=name).first() is not None:
            name = f"{name}-{''.join(random.sample(string.ascii_letters, 3))}"
        meeting = cls(user=user, organization=user.organization, name=name)
        cls.store(meeting)
        return meeting

    @classmethod
    def find_and_delete(cls, room_id):
        room = cls.query.filter_by(id=room_id).first()
        if room is not None:
            cls.delete(room)
            return True
        return False

    def serialize(self):
        data = dict()
        for key in self.__json_fields:
            data[key] = getattr(self, key, '')
        return data

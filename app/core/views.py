from flask import Blueprint, current_app, request, jsonify
from werkzeug.local import LocalProxy

from app.constants import ROWS_PER_PAGE
from app.core.models import MeetingRoom
from lib.decorators import require_apikey, require_json

bp = Blueprint('rooms', __name__)
logger = LocalProxy(lambda: current_app.logger)


@bp.route('/get/<int:organization_id>', methods=['GET'])
@bp.route('/get', methods=['GET'])
@require_apikey
def get_rooms(organization_id=None):
    logger.info(f'fetching rooms list')
    page = request.args.get('page', 1, type=int)
    qty = request.args.get('qty', ROWS_PER_PAGE, type=int)
    rooms = MeetingRoom.query

    if organization_id:
        rooms = rooms.filter_by(organization_id=organization_id)

    rooms = rooms.paginate(page=page, per_page=qty)
    return jsonify(
        {
            'current_page': rooms.page,
            'items': [item.serialize() for item in rooms.items],
            'pages': rooms.pages,
            'total': rooms.total
        }
    ), 200


@bp.route('/create', methods=['POST'])
@require_apikey
@require_json
def create_room():
    logger.info(f'creating room data: {request.json}')
    room = MeetingRoom.create(request.json)
    return jsonify(room.serialize()), 201


@bp.route('/delete/<int:room_id>', methods=['DELETE'])
@require_apikey
def delete_room(room_id):
    logger.info(f'deleting room id: {room_id}')
    result = MeetingRoom.find_and_delete(room_id)
    if result:
        message = f"room with id {room_id} deleted successfully"
    else:
        message = f"Room with id {room_id} not found"
    return jsonify({'message': message})

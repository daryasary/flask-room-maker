from functools import wraps

from flask import abort, current_app, request


# TODO: Authentication method is just mocked and should be implemented
#  completely later
def require_apikey(view_function):
    @wraps(view_function)
    def decorated_function(*args, **kwargs):
        api_key = request.headers.get('x-api-key', None)
        if api_key is not None and api_key == current_app.config['API_KEY']:
            return view_function(*args, **kwargs)
        else:
            abort(401)

    return decorated_function


def require_json(view_function):
    @wraps(view_function)
    def decorated_function(*args, **kwargs):
        content_type = request.headers.get('Content-Type')
        if content_type is not None and content_type == 'application/json':
            return view_function(*args, **kwargs)
        else:
            abort(415)

    return decorated_function

# Flask Room Maker
Simple flask application which let users from different organizations, create their rooms. This sample applications
consists of 3 separate API for create/get(list)/delete rooms.

### Todo 
Authentication is not implemented and only a mock method is checking users credentials.

### Run project
To run project at first create .env file using sample.env file in this repository:

    cp sample.env .env 

Then make all your changes there, create python virtualenv and install requirements:

    pip install -r requirements.txt

And for running the webserver run:

    python run.py
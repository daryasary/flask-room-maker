import pytest
from app import create_app, db


@pytest.fixture()
def app():
    app = create_app()
    yield app
    with app.app_context():
        db.drop_all()


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_app_crud_views(client):
    response = client.get("/api/v1/rooms/get")
    assert response.status == '401 UNAUTHORIZED'

    get_response = client.get(
        "/api/v1/rooms/get",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert get_response.json.get('items', None) == []
    assert get_response.json.get('pages', 1) == 0
    assert get_response.json.get('total', 1) == 0

    create_response = client.post(
        "/api/v1/rooms/create",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert create_response.status == '415 UNSUPPORTED MEDIA TYPE'

    create_response = client.post(
        "/api/v1/rooms/create", json={},
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert create_response.status == '201 CREATED'
    assert create_response.json.keys() == {'id', 'user_id', 'name',
                                           'organization_id', 'created_at'}

    get_response = client.get(
        "/api/v1/rooms/get",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert get_response.json.get('total', 0) == 1

    delete_response = client.delete(
        "/api/v1/rooms/delete/14",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert delete_response.json.get('message',
                                    "") == "Room with id 14 not found"

    delete_response = client.delete(
        "/api/v1/rooms/delete/1",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert delete_response.json.get('message',
                                    "") == "room with id 1 deleted successfully"

    get_response = client.get(
        "/api/v1/rooms/get",
        headers={"x-api-key": "0zghimu#@Wxs0eyXaXa#!m!ZFzC^Ma"}
    )
    assert get_response.json.get('total', 1) == 0
